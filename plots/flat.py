from  .. import base
tk = base.tk
from  .image import DetImgPlot, ImgHistFrame


class FlatPlot(DetImgPlot):
    kw_allaxes = { "hist":{"figure":"flat_hist"},
                    "img":{"figure":"flat_imshow"}
                }
    
    def initData(self, data):
        self.on_refresh = base.Command()
        self.on_refresh_section = base.Command()
        
        self.data = data
        data.on_image_change.append(self.refresh)
    def refresh_section(self):
        self.sectionplot = self.data.section
        self.on_refresh_section()
    def refresh(self):
        data = self.data
        self.setData(data.image.Signal(), sectionplot=data.section)
        self.kw["allaxes.img"]["title"] = "p=%d, DIT=%0.5f, I=%f"%(data.polar, data.dit, data.illumination)
        self.on_refresh()
        
class FlatFrame(base.BaseFrame):
    def __init__(self, parent, data, pack=None, kw=None, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        plot = FlatPlot()
        plot.initData(data)
        self.plot = plot
        self.plotFrame = ImgHistFrame(self, plot)
        plot.on_refresh.append(self.replot)
        plot.on_refresh_section.append(self.replot)
        data.on_section_change.append(self.plot.refresh_section)
        
        
        self.plotFrame.pack(side=tk.TOP)
    def is_selected(self):
        return True
        #return self.data.config["selected_frame"] == "flat"
    def replot(self):
        if self.is_selected():
            self.plotFrame.replot()
    def refresh(self):
        if self.is_selected():
            self.plot.refresh()
        

